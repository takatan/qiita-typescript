import React from "react";

const now = new Date().toLocaleString();
const DemoImportSync: React.FC = () => <div>{now}</div>;
export default DemoImportSync;
