import React, { useState } from "react";
import { DemoReduxUseReducer } from "./demo-redux-use-reducer";
import { DemoReduxReactRedux } from "./demo-redux-react-redux";
import { DemoReduxReduxHooks } from "./demo-redux-redux-hooks";

const UseState: React.FC = () => {
  const [text, setText] = useState("");
  return (
    <div>
      value={text}
      <input
        type="text"
        value={text}
        onChange={event => setText(event.target.value)}
      />
    </div>
  );
};

export const DemoRedux: React.FC = () => {
  return (
    <div>
      UseState
      <UseState />
      DemoReduxReactRedux
      <DemoReduxReactRedux />
      DemoReduxUseReducer
      <DemoReduxUseReducer />
      DemoReduxReduxHooks
      <DemoReduxReduxHooks />
    </div>
  );
};
