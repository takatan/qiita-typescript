import { createStore, Dispatch } from "redux";
import React from "react";
import { connect, Provider } from "react-redux";

const SET_TEXT = "SET_TEXT";

interface SetTextAction {
  type: typeof SET_TEXT;
  payload: string;
}

type Action = SetTextAction;

interface State {
  text: string;
}

const reducer = (state: State = { text: "" }, action: Action): State => {
  switch (action.type) {
    case "SET_TEXT":
      return { text: action.payload };
    default:
      return state;
  }
};

const store = createStore<State, Action, {}, {}>(reducer);

interface AppStateProps {
  text: string;
}

interface AppDispatchProps {
  setText: (s: string) => void;
}

const App: React.FC<AppStateProps & AppDispatchProps> = props => {
  const { text, setText } = props;
  return (
    <div>
      value={text}
      <input
        type="text"
        value={text}
        onChange={event => setText(event.target.value)}
      />
    </div>
  );
};

export const DemoReduxReactRedux = () => {
  const RApp = connect(
    (initialState: State): AppStateProps => ({
      text: initialState.text
    }),
    (dispatch: Dispatch<Action>): AppDispatchProps => ({
      setText: (i: string) => dispatch({ type: SET_TEXT, payload: i })
    })
  )(App);
  return (
    <Provider store={store}>
      <RApp />
    </Provider>
  );
};
