import { createStore } from "redux";
import React from "react";
import { Provider, useDispatch, useSelector } from "react-redux";

const SET_TEXT = "SET_TEXT";

interface SetTextAction {
  type: typeof SET_TEXT;
  payload: string;
}

type Action = SetTextAction;

interface State {
  text: string;
}

const reducer = (state: State = { text: "" }, action: Action): State => {
  switch (action.type) {
    case "SET_TEXT":
      return { text: action.payload };
    default:
      return state;
  }
};

const store = createStore<State, Action, {}, {}>(reducer);

const App: React.FC = () => {
  const text = useSelector((state: State) => state.text);
  const dispatch = useDispatch();
  return (
    <div>
      value={text}
      <input
        type="text"
        value={text}
        onChange={event =>
          dispatch({ type: SET_TEXT, payload: event.target.value })
        }
      />
    </div>
  );
};

export const DemoReduxReduxHooks = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};
