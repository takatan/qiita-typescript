import React, { useReducer } from "react";

const SET_TEXT = "SET_TEXT";

interface SetTextAction {
  type: typeof SET_TEXT;
  payload: string;
}

type Action = SetTextAction;

interface State {
  text: string;
}

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case "SET_TEXT":
      return { text: action.payload };
    default:
      throw new Error("illegal action");
  }
};

const initialState: State = { text: "" };

export const DemoReduxUseReducer: React.FC = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <div>
      value={state.text}
      <input
        type="text"
        value={state.text}
        onChange={event =>
          dispatch({ type: SET_TEXT, payload: event.target.value })
        }
      />
    </div>
  );
};
