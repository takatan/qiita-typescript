import React, { useState } from "react";

export const DemoInput: React.FC = () => {
  const ValueOnly: React.FC = () => (
    <div>
      <label>nothing</label>
      {/*<input type="text" value="0123" />*/}
    </div>
  );
  const DefaultValue: React.FC = () => (
    <div>
      <label>defaultValue</label>
      <input type="text" defaultValue="abcd" />
    </div>
  );
  const ValueWithOnChange: React.FC = () => {
    const [str, setStr] = useState("0123");
    return (
      <div>
        <label>value and handler: {str}</label>
        <input
          type="text"
          value={str}
          onChange={event => setStr(event.target.value)}
        />
      </div>
    );
  };
  const DefaultValueWithOnChange: React.FC = () => {
    const [str, setStr] = useState("0123");
    return (
      <div>
        <label>defaultValue and handler: {str}</label>
        <input
          type="text"
          defaultValue="abcd"
          onChange={event => setStr(event.target.value)}
        />
      </div>
    );
  };

  const DefaultValueWithOnChange2: React.FC = () => {
    const [str, setStr] = useState("0123");
    return (
      <div>
        <label>defaultValue and handler 2: {str}</label>
        <input
          type="text"
          defaultValue={str}
          onChange={event => setStr(event.target.defaultValue)}
        />
      </div>
    );
  };
  return (
    <div>
      <ValueOnly />
      <DefaultValue />
      <ValueWithOnChange />
      <DefaultValueWithOnChange />
      <DefaultValueWithOnChange2 />
    </div>
  );
};
