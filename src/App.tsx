import React from "react";
import "./App.css";
import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import { DemoReduxPromise } from "./demo-redux-promise";
import { array } from "fp-ts/lib/Array";
import DemoImportSync from "./demo-import-sync";
import loadable from "loadable-components";
import { DemoInput } from "./demo-input";
import { DemoRedux } from "./demo-redux";

interface Page {
  url: string;
  component: any;
}

const LoadableComponent = loadable(() => import("./demo-import-async"));

const App: React.FC = () => {
  const pages: Array<Page> = [
    { url: "/", component: <span /> },
    { url: "/redux-promise", component: <DemoReduxPromise /> },
    { url: "/input", component: <DemoInput /> },
    { url: "/redux", component: <DemoRedux /> }
  ];
  return (
    <div className="App">
      <BrowserRouter>
        <div>
          {array.mapWithIndex(pages, (i, page) => (
            <span key={i}>
              [<Link to={page.url}>{page.url}</Link>]
            </span>
          ))}
          <div>
            <Link to="/import-sync">import-sync</Link>/
            <Link to="/import-async">import-async</Link>
          </div>
        </div>
        <Switch>
          {array.mapWithIndex(pages, (i, page) => (
            <Route key={i} path={page.url} exact={true}>
              url={page.url}
              <hr />
              {page.component}
            </Route>
          ))}
          <Route path="/import-sync" exact={true}>
            <hr />
            <DemoImportSync />
          </Route>
          <Route path="/import-async" exact={true}>
            <hr />
            <LoadableComponent />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default App;
