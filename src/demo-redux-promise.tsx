import React from "react";
import { connect, Provider } from "react-redux";
import { createStore, Dispatch } from "redux";
import axios from "axios";

interface StateProps {
  time: string;
}

interface DispatchProps {
  setTime: () => void;
  reset: () => void;
}

const App: React.FC<StateProps & DispatchProps> = props => (
  <div>
    <h1>time = {props.time}</h1>
    <button onClick={props.setTime}>Get</button>
    <button onClick={props.reset}>Reset</button>
  </div>
);

const RESET = "RESET";
const SET_TIME = "SET_TIME";

interface Reset {
  type: typeof RESET;
}

interface SetTime {
  type: typeof SET_TIME;
  payload: string;
}

type Action = Reset | SetTime;

interface State {
  time: string;
}

const reducer = (state: State = { time: "" }, action: Action): State => {
  switch (action.type) {
    case "RESET":
      return { time: "" };
    case "SET_TIME":
      return { time: action.payload };
    default:
      return state;
  }
};

const mapStateToProps = (state: State): StateProps => ({
  time: state.time
});

// Promiseを返すアクション
const setTime = async (): Promise<SetTime> => {
  const response = await axios.get("https://ntp-a1.nict.go.jp/cgi-bin/json");
  const time = response.data.st;
  return {
    type: SET_TIME,
    payload: time
  };
};

const reset = (): Reset => ({
  type: RESET
});

// このようにdispatchを呼ぶときにawait asyncをつけるだけでPromiseを返すactionが普通に使えるようになる
const mapDispatchToProps = (dispatch: Dispatch<Action>): DispatchProps => ({
  setTime: async () => dispatch(await setTime()),
  reset: () => dispatch(reset())
});

const store = createStore<State, Action, {}, {}>(reducer, { time: "" });
const RApp = connect(mapStateToProps, mapDispatchToProps)(App);

export const DemoReduxPromise = () => (
  <Provider store={store}>
    <RApp />
  </Provider>
);
