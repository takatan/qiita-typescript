import React from "react";

const now = new Date().toLocaleString();
const DemoImportAsync: React.FC = () => <div>{now}</div>;
export default DemoImportAsync;
